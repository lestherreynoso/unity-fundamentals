﻿using UnityEngine;
using System.Collections;

public class ObjectColor : MonoBehaviour
{
	public Color changedColor = Color.white;
	private Color originalColor = Color.white;

	void Awake ()
	{
		originalColor = this.renderer.material.color;
	}
	void Update ()
	{
		if ( Input.GetKeyDown (KeyCode.G) )
		{
			EnableColor ();
		}
		else if ( Input.GetKeyUp (KeyCode.G) )
		{
			DisableColor ();
		}
	}

	public void EnableColor ()
	{
		this.renderer.material.color = changedColor;
	}

	public void DisableColor ()
	{
		this.renderer.material.color = originalColor;
	}









}
