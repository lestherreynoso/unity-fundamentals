﻿using UnityEngine;
using System.Collections;

public class MouseClicks : MonoBehaviour
{
	private Color originalColor = Color.white;

	void Awake ()
	{
		originalColor = this.renderer.material.color;
	}

	void OnMouseEnter ()
	{
		this.renderer.material.color = Color.red;
	}

	void OnMouseExit ()
	{
		this.renderer.material.color = originalColor;
	}
}