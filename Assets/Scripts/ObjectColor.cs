﻿using UnityEngine;
using System.Collections;

public class ObjectColor : MonoBehaviour {

	public Color changedColor = Color.white;
	Color originalColor = Color.white;
	Renderer rend;

	void Awake(){
		rend = this.GetComponent<Renderer> ();

//		originalColor = this.renderer.material.color;
		originalColor = rend.material.color;
	}
	void Start(){
	}

	void Update (){
		if (Input.GetKeyDown (KeyCode.G)) {
			EnableColor ();

		} else if (Input.GetKeyUp (KeyCode.G)) {
			DisableColor ();
		}
	}

	public void EnableColor() {
		rend.material.color = changedColor;
	}

	public void DisableColor() {
		rend.material.color = originalColor;
	}

}
