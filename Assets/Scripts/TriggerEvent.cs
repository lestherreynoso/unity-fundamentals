﻿using UnityEngine;
using System.Collections;

public class TriggerEvent : MonoBehaviour {

	public GameObject lightBulb = null;
	public GameObject wall = null;
	public AudioClip lightBulbOn = null;
	AudioSource audio;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (other.name == "player") {
			//turn on light bulb
			lightBulb.SetActive(true);

			audio.PlayOneShot (lightBulbOn);

			wall.GetComponent<ObjectScaling> ().EnableScale ();
			wall.GetComponent<ObjectColor> ().EnableColor ();
		}
	}

	void OnTriggerExit(Collider other){
		if (other.name == "player") {
			//turn on light bulb
			lightBulb.SetActive(false);

			audio.Stop ();

			wall.GetComponent<ObjectScaling> ().DisableScale ();
			wall.GetComponent<ObjectColor> ().DisableColor ();

		}
	}
}
