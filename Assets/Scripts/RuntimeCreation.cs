﻿using UnityEngine;
using System.Collections;

public class RuntimeCreation : MonoBehaviour {

    public PrimitiveType primitiveType = PrimitiveType.Cube;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.A))
        {
            CreativePrimitiveCube();
        }
	}

    void CreativePrimitiveCube()
    {
        GameObject obj = GameObject.CreatePrimitive(primitiveType);
        obj.AddComponent<Rigidbody>();
    }
}
