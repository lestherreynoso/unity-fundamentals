﻿using UnityEngine;
using System.Collections;

public class CameraTriggers : MonoBehaviour {

	public Cameras cameras;
	public Cameras.CameraState cameraState;


	void OnTriggerEnter(Collider other){
		Debug.Log ("Object: " + other.name + " entered trigger.");
		if (other.name == "player"){

			cameras.cameraState = cameraState;
		}
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
