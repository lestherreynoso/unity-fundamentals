﻿using UnityEngine;
using System.Collections;

public class ObjectScaling : MonoBehaviour {

	public float scaleAmount = 3.0f;
	public Vector3 originalScale = Vector3.zero;
	
	void Awake(){
		originalScale = this.transform.localScale;
	}
	void Update () {
		if (Input.GetKeyDown (KeyCode.G)) {
			Debug.Log ("Key G Pressed: Scale");
			EnableScale ();
		}
		else if (Input.GetKeyDown (KeyCode.F)) {
			Debug.Log ("Key F Pressed: Scale");
			DisableScale ();
		}
	}

	public void EnableScale(){
		this.transform.localScale = Vector3.one *scaleAmount;
	}

	public void DisableScale(){
		this.transform.localScale = originalScale;
	}
}
