﻿using UnityEngine;
using System.Collections;

public class MouseClicks : MonoBehaviour {

	Renderer rend;
	Color originalColor = Color.white;

	void Awake(){
		rend = this.GetComponent<Renderer> ();
		originalColor = rend.material.color;
	}

	void OnMouseEnter(){
		Debug.Log ("Mouse Entered");
		rend.material.color = Color.red;
	}

	void OnMouseExit(){
		Debug.Log ("Mouse Exited");
		rend.material.color = originalColor;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
